const Moralis = require('moralis/node');
const STATE = require('./State.js');

require('dotenv').config();

Moralis.initialize(process.env.MORALIS_APPLICATION_ID, process.env.MORALIS_MASTER_KEY);
Moralis.serverURL = process.env.MORALIS_SERVER_URL;

const CarbonNFT = Moralis.Object.extend("CarbonNFT");
const query = new Moralis.Query(CarbonNFT);

module.exports = class CarbonNFTHolder {

    #carbonNFT;

    /**
     * builder for CarbonNFTHolder
     * @param {String} nft_code identifier code of NFT
     * @param {String} issuer who issues carbon credit
     * @param {String} owner wallet address of owner
     * @param {Number} emission_volume how many emission does this NFT holds
     * @param {String} price the price for NFT
     * @param {String} state state of NFT {STATE.BUYABLE, STATE.PROCESSABLE, STATE.CLOSEABLE}
     */
    constructor(nft_code, issuer, owner, emission_volume, price, state){
        this.#carbonNFT = new CarbonNFT();

        // define CarbonCredit structure
        this.#carbonNFT.set("nft_code", nft_code);
        this.#carbonNFT.set("issuer", issuer);
        this.#carbonNFT.set("owner", owner);
        this.#carbonNFT.set("emission_volume", emission_volume);
        this.#carbonNFT.set("price", price);
        this.#carbonNFT.set("state", state);
    }

    /**
     * get CarbonNFT from database
     * @param {String} nft_code identifier code of NFT
     * @return CarbonCreditHolder instance that holds carbonNFT
     */
    static async getCarbonNFT(nft_code) {
        
    }

    //=============== CarbonNFT database ===============

    /**
     * check NFT Existed
     * @param {String} nft_code identifier code of NFT
     * @returns true if nft_code exists, false if not
     */
    static async checkNFTExisted(nft_code) {
        query.equalTo("nft_code", nft_code);
        let result = await query.first();

        if(result) return true;
        
        return false;
    }   

    /**
     * save CarbonNFT to database 
     * @returns id if successful else null
     */
    async saveCarbonNFT() {
        let dataID = null;

        await this.#carbonNFT
            .save()
            .then((data) => {
                dataID = data.id;
            })
            .catch((error) => {
                console.log(error);
            });
        
        return dataID;
    }

    /**
     * unload CarbonNFT from database
     */
    async unloadCarbonNFT() {

    }

    //=============== CarbonNFT getter ===============
    //cannot return the whole #carbonNFT, if I do so, caller can easily change and set value of carbonNFT
    //Therefore, I will only return each element of carbonNFT

    /**
     * get nft_code of CarbonNFT
     * @returns nft_code
     */
    getNFT_CODE(){
        return this.#carbonNFT.get("nft_code");
    }

    /**
     * get state of CarbonNFT
     */
    getState() {
        return this.#carbonNFT.get('state');
    }

    //=============== CarbonNFT setter ===============

    /**
     * an NFT is immutable in nature. Only price is allowed to update.
     * @param {String} price 
     * @returns id if successful else null
     */
    updatePrice(price) {
        this.#carbonNFT.set("price", price);
    }

    /**
     * update state of CarbonNFT
     * @param {String} state state of NFT {STATE.BUYABLE, STATE.PROCESSABLE, STATE.CLOSEABLE}
     */
    updateState(state) {
        this.#carbonNFT.set("state", state);
    }

    /**
     * update owner of CarbonNFT
     * @param {String} owner 
     */
    updateOwner(owner){
        this.#carbonNFT.set("owner", owner);
    }

}