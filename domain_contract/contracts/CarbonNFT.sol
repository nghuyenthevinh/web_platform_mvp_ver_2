pragma solidity >=0.4.22 <0.9.0;

import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

contract CarbonNFT is ERC721URIStorage, Ownable {
    using Counters for Counters.Counter;
    Counters.Counter private _tokenIds;

    constructor() ERC721("CarbonNFT", "SETS") {}

    function mint(address to, uint256 tokenId) public onlyOwner {
      _safeMint(to, tokenId, "");
      _tokenIds.increment();
    }

    function transfer(address from, address to, uint256 tokenId) internal {
      _safeTransfer(from, to, tokenId, "");
    }

}